%global fontname nerd

Name:           pm-graph
Version:        5.10
Release:        1%{?dist}
Summary:        Suspend/resume/boot timing analysis tools

License:        GPLv2
URL:            https://github.com/intel/pm-graph
Source0:        %{name}-%{version}.tar.gz

BuildArch:		noarch

BuildRequires:  make

Requires:       coreutils
Requires:       grep
Requires:       mcelog
Requires:       pciutils
Requires:       usbutils
Requires:       kernel-tools
Requires:       python3
Requires:       python3-requests


%description
pm-graph is designed to assist kernel and OS developers in optimizing their
linux stack's suspend/resume & boot time. Using a kernel image built with a few
extra options enabled, the tools will execute a suspend or boot, and will
capture dmesg and ftrace data. This data is transformed into a set of timelines
and a callgraph to give a quick and detailed view of which devices and kernel
processes are taking the most time in suspend/resume & boot.


%prep
rm -rf %{_builddir}/%{name}-%{version}/*
%autosetup -p 1


%build


%install
DESTDIR=%{buildroot} make install


%files
%{_bindir}/*
%{_prefix}/lib/*
%{_datadir}/*


%changelog
* Tue Dec 13 2022 Freeman Zhang <zhanggyb@lenovo.com> - 5.10-1
- Upgrade to 5.10

* Fri Jul 1 2022 Freeman Zhang <zhanggyb@lenovo.com> - 5.9-3
- Add missing required kernel-tools which includes turbostat

* Fri Jul 1 2022 Freeman Zhang <zhanggyb@lenovo.com> - 5.9-2
- Add missing requirements

* Thu Jun 30 2022 Freeman Zhang <zhanggyb@lenovo.com> - 5.9-1
- Initial build
